# BloggerGen

## Idea

A python based static site generator, focused on bloggin applications.

## Implementation

Yaml files are used for storing the various configurations and customisations of the website.

Markdown is used for writing the blog posts and the content for other pages.

A pug/jade look-alike might be used to define some templates, like how the the header should look etc. But these might also be hardcoded.

Since this is a fun/hobby (past-time) project, intended to learn; everything will be implemented in pure Python.
